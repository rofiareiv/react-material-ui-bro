import React from 'react';
// import logo from './logo.svg';
// import './App.css';
import { createBrowserHistory } from 'history';
import { Router, Route, Redirect } from 'react-router-dom';

import Layout from 'layouts/Admin';

const histori = createBrowserHistory();

function App() {
  return (
    <Router history={histori}>
      <Route path="/" component={Layout} />
      <Redirect from="/" to="/dashboard" />
    </Router>
  );
}

export default App;
