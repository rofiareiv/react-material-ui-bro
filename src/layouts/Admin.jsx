import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import clsx from 'clsx';
import classNames from 'classnames';
// import { useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import useStyles from 'assets/jss/layout/adminStyle';

import routers from 'routes';

const switchRoute = (
  <Switch>
    {routers.map((prop, key) => {
      return (
        <Route
          path={prop.path}
          component={prop.component}
          key={key}
        />
      )
    })}
  </Switch>
)

const Layout = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Bro Admin
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          {routers.map((prop, key) => {
            return (
              <NavLink className={classes.item} to={prop.path} key={key}>
                <ListItem button className={classes.itemLink}>
                  {typeof prop.icon === "string" ? (
                    <ListItemIcon className={classes.itemIcon}>{prop.icon}</ListItemIcon>
                  ) : (
                    <prop.icon className={classes.itemIcon} />
                  )}
                  <ListItemText primary={prop.name} />
                </ListItem>
              </NavLink>
            )
          })}

          {/* {routers.map((prop, key) => {
            return (
              <ListItem button key={key}>
                {typeof prop.icon === "string" ? (
                  <ListItemIcon>{prop.icon}</ListItemIcon>
                ):(
                  <prop.icon />
                )}
                <ListItemText primary={prop.name} />
              </ListItem>
            )
          })} */}
        </List>
        <Divider />
        <List>
          {['All mail', 'Trash', 'Spam'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {/* =========== content here ============ */}
        {switchRoute}
        {/* ============ end content ============ */}
      </main>
    </div>
  );
}

export default Layout;
