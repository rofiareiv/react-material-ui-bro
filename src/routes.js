import Home from '@material-ui/icons/Home';
import Map from '@material-ui/icons/MapOutlined';

import Dashboard from "views/Dashboard/Dashboard.jsx";
import Production from "views/Production/Production.jsx";

const adminRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Home,
    component: Dashboard
  },
  {
    path: "/production",
    name: "Production",
    icon: Map,
    component: Production
  }
];

export default adminRoutes;
