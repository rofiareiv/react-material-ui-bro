import axios from 'axios';

const BASE_URL = 'http://103.105.98.211:8004/';

const getAuthToken = async (username, password) => {
  let path = 'auth-jwt/';
  const res = await axios.post(`${BASE_URL}${path}`, {
    username: username,
    password: password
  });

  return res;
}

const Get = (path) => {
  const promise = new Promise((resolve, reject) => {
    axios.get(`${BASE_URL}${path}`)
    .then((result) => { 
      resolve(result.data);
    }, (err) => {
      reject(err);
    })
  })

  return promise;
}

const getAllAccount = () => Get('api/account/');

const API = {
  getAuthToken,
  getAllAccount 
}

export default API;